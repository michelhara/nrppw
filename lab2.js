const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
var fs = require('fs');
const app = express();
const http = require('http').createServer(app);
var https = require('https').createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert'),
  requestCert: false,
  rejectUnauthorized: false
}, app);

const svgCaptcha = require('svg-captcha');

const dotenv = require('dotenv');
dotenv.config();

app.use(express.static('public'));
app.use(bodyParser());
app.use("/public", express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs')

const port = process.env.PORT || 4080;

const users = [
  {name: "admin", password: "admin", role: "admin"},
  {name: "user", password: "user", role: "user"}
];

var currentUser = null;
var captchaText = null;
var secure = true;

function authorize(username, password) {
  if (username === users[0].name) {
    return password === users[0].password;
  } else if (username === users[1].name) {
    return password === users[1].password;
  } else {
    return false;
  }
}

app.get('', (req, res) => {
  res.render('index');
});

app.get('/menu', (req, res) => {
  res.render('menu');
})

app.post('/menu', (req, res) => {
  secure =  req.body.version == "secure";
  res.render('menu')
});

app.get('/admin', (req, res) => {
  if (secure) {
    if (currentUser == "admin") {
      res.render('admin', {users: users});    
    } else if (currentUser == "user") {
      res.render('forbidden');
    } else {
      res.render('notAuthorized');
    }
  } else {
    res.render('admin', {users: users});  
  } 
});

app.get('/user', (req, res) => {
  if (secure) {
    if (currentUser == "user") {
      res.render('user');    
    } else if (currentUser == "admin") {
      res.render('forbidden');
    } else {
      res.render('notAuthorized');
    }
  } else {
    res.render('user');
  }
});

app.get('/login',  (req, res) => {
  var captcha = svgCaptcha.create();
  captchaText = captcha.text;

  if (req.query.unsuccessfullLoginSecure) {
    res.render('loginPage', {loginError: "Wrong username/password.", captcha: captcha});
  } else if(req.query.captchaFail) {
    res.render('loginPage', {loginError: "Captcha failed.", captcha: captcha});
  } else if (req.query.unsuccessfullLoginVulnerable) {
    res.render('loginPage', {loginError: "Wrong username.", captcha: secure ? captcha : undefined});
  } else if (req.query.invalidPwd) {
    res.render('loginPage', {loginError: "Wrong password.", captcha: secure ? captcha : undefined});
  } else {
    res.render('loginPage', {loginError: "", captcha: secure ? captcha : undefined});
  }
});

app.post('/login', (req, res) => {
  if (secure) {

    if (req.body.captcha != captchaText) {
      currentUser = null;
      res.redirect('/login/?captchaFail=true');
    }

    const authSuccess = authorize(req.body.username, req.body.password);
    
    if (authSuccess) {
      if (req.body.username === users[0].name) {
        currentUser = users[0].name;
        res.redirect('/admin');
      } else if (req.body.username === users[1].name) {
        currentUser = users[1].name;
        res.redirect('/user');
      }
    } else {
      currentUser = null;
      res.redirect('/login/?unsuccessfullLoginSecure=true');
    }
    
  } else {
    if (req.body.username === users[0].name) {
      if (req.body.password === users[0].password) {
        currentUser = users[0].name;
        res.redirect('/admin');
      } else {
        res.redirect('/login/?invalidPwd=true');
      }
    } else if (req.body.username === users[1].name) {
      if (req.body.password === users[1].password) {
        currentUser = users[1].name;
        res.redirect('/user');
      } else {
        res.redirect('/login/?invalidPwd=true');
      }
    } else {
      currentUser = null;
      res.redirect('/login/?unsuccessfullLoginVulnerable=true');
    }
  }

});

app.post('/logout', (req, res) => {
  currentUser = null;
  res.redirect('/login');
});
 
app.get('/xxe', (req, res) => {
  const xml = 
    `<?xml version="1.0" encoding="utf-8">
    <breakfast_menu>
      <food>
        <name>Belgian Waffles</name>
        <price>$5.95</price>
        <description>Two of our famous Belgian Waffles with plenty of real maple syrup</description>
        <calories>650</calories>
      </food>
      <food>
        <name>French Toast</name>
        <price>$4.50</price>
        <description>Thick slices made from our homemade sourdough bread</description>
        <calories>600</calories>
      </food>
      <script>javascript:alert("XXE attack executed!");</script>
      <script>
        //fetch the file and write it's content in the window and console
        fetch('./public/textFile.txt')
        .then(
          function(response) {
            response.text().then(function(data) {
              console.log(data);
              document.write(data);
            });
          }
        );
      </script>
    </breakfast_menu>`;
  res.render('xxe', {xml: xml});
});

app.post('/xxe', (req, res) => {
  var outputXML = req.body.xml;

  if (secure && outputXML.trim() != "") {
    outputXML = outputXML.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;");
    outputXML = outputXML.replace(/&(?!(amp;)|(lt;)|(gt;)|(quot;)|(#39;)|(apos;))/g, "&amp;");
    outputXML = outputXML.replace(/([^\\])((\\\\)*)\\(?![\\/{])/g, "$1\\\\$2");  //replaces odd backslash(\\) with even.
  }

  res.render('xxe_result', {xml: outputXML})
});

if (process.env.PORT) {
  http.listen(process.env.PORT);
} else { 
  https.listen(port, function () {
    console.log(`Server running at https://localhost:${port}/`);
  });
}
